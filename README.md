# Prosemirror-selection-leaf-plugin


A plugin designed to help make the behaviour of selections within prosemirror behave more like those found in conventional desktop publishing suites. See the GIF below for a demo

<img src="https://gitlab.com/jaysaurus/prosemirror-selection-leaf-plugin/raw/master/selectionExample.gif">

## installation

```
npm i --save prosemirror-selection-leaf-plugin
```

add the following import and plugin to your code

```javascript
...
import { createSelectionLeafPlugin } from 'prosemirror-selection-leaf-plugin'

EditorState.create({
  doc: DOMParser.fromSchema(schema).parse(ed.doc),
  plugins: [
    createSelectionLeafPlugin()
    ...
  ]
})
```
By default, the plugin assumes that your prosemirror schema implementation supports
&lt;br/&gt; tags of the type "hardBreak".  Should your schema specify a different
name for &lt;br/&gt; tags, simply supply the name as an argument to the method:

```javascript
createSelectionLeafPlugin('myBreakPoint')
```

finally, **ensure** you add CSS as below to your main css file/style tag (though feel free to choose the colours!)

```CSS
::selection {
  background: #a8d1ff;
}
::-moz-selection {
  background: #a8d1ff;
}

.selection-after-edge::after,
.selection-before-edge::before {
  content: ' ';
  background: #a8d1ff;
}
```
