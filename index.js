import { SelectionLeafFactory } from './selection-leaf.js'

export const { createSelectionLeafPlugin } = SelectionLeafFactory()
