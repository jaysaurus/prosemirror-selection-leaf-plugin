import stampit from 'stampit'

export const PluginHelper = stampit({
  props: {
    ops: {
      eq: (a, b) => a === b,
      gt: (a, b) => a > b,
      gte: (a, b) => a >= b,
      lt: (a, b) => a < b,
      lte: (a, b) => a <= b,
      ne: (a, b) => a !== b
    }
  },
  init () {
    this.positionIsWithinSelection =
      function (position, { $anchor, $head }, useEquality = false) {
        const op = {
          gt: useEquality ? this.ops['gte'] : this.ops['gt'],
          lt: useEquality ? this.ops['lte'] : this.ops['lt']
        }
        return (op.lt($anchor.before(), $head.before()) &&
          op.gt(position, $anchor.before()) &&
          op.lt(position, $head.before())) ||
          (op.gt($anchor.before(), $head.before()) &&
            op.gt(position, $head.before()) &&
            op.lt(position, $anchor.before()))
      }.bind(this)
  }
})
