import stampit from 'stampit'

import { Plugin } from 'prosemirror-state'
import { Decoration, DecorationSet } from 'prosemirror-view'

import { PluginHelper } from './plugin-helper.js'

export const SelectionLeafFactory = stampit(PluginHelper, {
  init () {
    let initialised = false

    function hilightAfterEdge (decorations, selection) {
      if (selection.parent &&
        selection.after() > 1 &&
        selection.parent.content.size === selection.parentOffset) {
        decorations.push(
          Decoration.inline(
            selection.after() - 2,
            selection.after() - 1,
            { class: 'selection-after-edge' }))
        if (selection.pos > 0) {
          decorations.push(
            Decoration.node(
              selection.pos - 1,
              selection.pos,
              { class: 'selection-before-edge' }))
        }
      }
    }

    function hilightBreakFromAnchor (decorations, selection, pos) {
      if (pos > 0 &&
        pos >= selection.$anchor.pos &&
        pos <= selection.$head.pos) {
        decorations.push(
          Decoration.inline(
            pos,
            pos + 1,
            {
              class: 'selection-before-edge',
              nodeName: 'span'
            }))
      }
    }

    function hilightBreakFromHead (decorations, selection, pos) {
      if (pos > 0 &&
        pos >= selection.$head.pos &&
        pos <= selection.$anchor.pos) {
        decorations.push(
          Decoration.inline(
            pos - 1,
            pos,
            {
              class: 'selection-after-edge',
              nodeName: 'span'
            }))
      }
    }

    const hilightEmptyIntermediateNodes =
      function (decorations, selection, node, pos) {
        if (pos > 0 &&
          pos !== selection.$anchor.before() &&
          pos !== selection.$head.before() &&
          this.positionIsWithinSelection(pos, selection)) {
          decorations.push(
            Decoration.node(
              pos,
              pos + 2,
              { class: 'selection-before-edge' }))
        }
      }.bind(this)

    function hilightBeforeEdge (decorations, selection, s, i) {
      if (!selection[s].parentOffset &&
        (selection[s].pos > selection[i ? '$anchor' : '$head'].pos ||
          (!selection[s].parent.content.size))) {
        decorations.push(
          Decoration.node(
            selection[s].before(),
            selection[s].after(),
            { class: 'selection-before-edge' }))
      }
    }
    // --------------
    // PUBLIC METHODS
    // --------------
    this.createSelectionLeafPlugin = function (breakName = 'hardBreak') {
      return new Plugin({
        props: {
          decorations: ({ doc, selection }) => {
            if (initialised) {
              const decorations = []
              if (selection.$head !== selection.$anchor) {
                doc.descendants((node, pos) => {
                  if (node.isBlock) {
                    ['$anchor', '$head'].forEach((s, i) => {
                      if (selection[s].parentOffset === 0 ||
                        pos === selection[s].before()) {
                        hilightBeforeEdge(decorations, selection, s, i)
                      }
                      hilightAfterEdge(decorations, selection[s])
                    })
                    hilightEmptyIntermediateNodes(
                      decorations, selection, node, pos)
                  }
                  if (node.type.name === breakName) {
                    hilightBreakFromAnchor(decorations, selection, pos)
                    hilightBreakFromHead(decorations, selection, pos)
                  }
                })
              }
              return DecorationSet.create(doc, decorations)
            } else initialised = true
          }
        }
      })
    }
  }
})
